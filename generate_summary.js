#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const chapters = [
  'chapter_1',
];

let summary = `# Summary

* [Blue Moon](README.md)

`;

chapters.forEach(chapter => {
  const imageFiles = fs.readdirSync(chapter)
    .filter(fileName => path.extname(fileName) === '.png')
    .sort()
    .map(path.parse);

  summary += `* ${chapter.replace('_', ' ')}\n`;

  imageFiles.forEach(imageFile => {    
    const markdownFileName = `${imageFile.name}.md`;
    fs.writeFileSync(path.join(chapter, markdownFileName), `![${imageFile.base}](${imageFile.base})\n`);

    const pageNumber = parseInt(imageFile.name.split('_').pop());
    summary += `  * [page ${pageNumber}](${chapter}/${markdownFileName})\n`;
  });
});

fs.writeFileSync('SUMMARY.md', summary);

